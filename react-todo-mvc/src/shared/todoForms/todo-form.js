import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './todo-forms.css';
import { App } from './../../App'


export const TodoComponent = ({ }) => {
  <div>
    <TodoForm />
    <TodoList />
    <TodoEraseAll />
  </div>
};

export const TodoForm = ({ addTodo }) => {
  // Input tracker
  let input;
  return (
    <div>
      <input ref={node => {
        input = node;
      }} />
      <button onClick={() => {
        addTodo(input.value);
        input.value = '';
      }}>
        +
            </button>
    </div>
  );
};

export const Todo = ({ todo, remove }) => {
  // Each Todo
  return (
    <li onClick={() => {
      remove(todo.id)
    }}>
      {todo.text}
    </li >
  );
};

export const TodoList = ({ todos, remove }) => {
  // Map through the todos
  const todoNode = todos.map((todo) => {
    return (<Todo todo={todo} key={todo.id} remove={remove} />)
  });
  return (<ul>{todoNode}</ul>);
};


export const Title = () => {
  const titleStyle = {
    'textAlign': 'center',
    'color': 'white',
    // 'backgroundColor':
  }
  return (
    <div>
      <div>
        <h1>to-do </h1>
      </div>
    </div>
  );
};

export const TodoEraseAll = ({ buttonText, eraseAll, }) => {
  const eraseButtonStyle = {
    // 'backgroundColor': 'red',
    'borderRadius': '50%',
    'height': '100px',
    'width': '100px',
    'border': 'solid 4px crimson'
  }

  return <div>
    <button
      onClick={eraseAll}
      className="erase-button"
      style={eraseButtonStyle}
    >
      {buttonText}
    </button>
  </div>;
};

export const TodoClock = () => {
  return (<div id="todo-clock">
    {new Date().toLocaleTimeString()}
  </div>)
}



