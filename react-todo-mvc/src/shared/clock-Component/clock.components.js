import React, { Component } from 'react';
import ReactDOM from 'react-dom';


class Clock extends React.Component {

  constructor(props) {
    super(props);
    this.state = { date: new Date() }
  }

  render() {
    return (
      <div id="todo-clock">
        {new Date().toLocaleTimeString()}
      </div>
    )
  }


}

setInterval(() => ReactDOM.render(<Clock />, document.getElementById('todo-clock')), 1000)

export default Clock;