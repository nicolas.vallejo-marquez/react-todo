import React, { Component } from 'react';

import logo from './logo.svg';
import './App.css';
import { TodoClock, TodoEraseAll, TodoForm, TodoList, Title, TodoComponent } from './shared/todoForms/todo-form';
import Clock from './shared/clock-Component/clock.components';

// Todo Id
window.id = 0;
class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      todoList: []
    }
  }

  addTodo(val) {
    const todo = { text: val, id: window.id++ }
    this.state.todoList.push(todo);
    this.setState({ data: this.state.todoList });
  }

  handleRemove(id) {
    const remainder =
      this.state.todoList.filter((todo) => {
        if (todo.id !== id) return todo;
      });
    this.setState({ todoList: remainder });
  }

  handleRemoveAll() {
    this.setState({ todoList: [] });
  }

  render() {
    // Render JSX
    return (
      <div>
        <Title />
        <Clock />
        <TodoForm
          addTodo={this.addTodo.bind(this)}
        />
        <TodoEraseAll
          class="cheese"
          buttonText='Borrar lista'
          eraseAll={this.handleRemoveAll.bind(this)} />
        <TodoList
          todos={this.state.todoList}
          remove={this.handleRemove.bind(this)}
        />

      </div>
    )
  }

}

export default App;
