import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { TodoClock } from './shared/todoForms/todo-form';
import registerServiceWorker from './registerServiceWorker';



ReactDOM.render(<App />, document.getElementById('container'));

registerServiceWorker();
